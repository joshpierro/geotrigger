package pierro.dallett.josh;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import pierro.dallett.josh.data.Feature;
import pierro.dallett.josh.data.FeatureList;
import pierro.dallett.josh.data.UserLocation;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;

public class LocationListeningService extends Service {
	
	boolean mRunFlag;
	private NotificationManager mNotificationManager;
	private int NOTIFICATION = R.string.local_service_started;
	String mGeofenceDetails = "";
	NotificationCompat.Builder mBuilder;
	MediaPlayer mMediaPlayer;
	double mLatitude;
	double mLongitude; 
	ArrayList<Feature> mFeatureList;
	
	final int mNotificationId = 1;
	
	public class LocalBinder extends Binder {
		LocationListeningService getService() {
            return LocationListeningService.this;
        }
    }
	
	   @Override
	    public void onCreate() {
		   
		    mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
	        showNotification();

	    }

	
	   @Override
		public int onStartCommand(Intent intent, int flags, int startId) {
		
			  mRunFlag = true;
			  
			  new Thread(new Runnable() {
			        public void run() {
			        	while(mRunFlag == true){
			        		 try {
			        			
			        			 Log.d("GETTING LAT/LNG", Double.toString(UserLocation.getInstance().Latitude) + " " + Double.toString(UserLocation.getInstance().Longitude));
				        	        
									if(mLatitude != UserLocation.getInstance().Latitude||
											mLongitude != UserLocation.getInstance().Longitude){
										
										  mLatitude = UserLocation.getInstance().Latitude;
					        			  mLongitude = UserLocation.getInstance().Longitude;
					        			  
					        			  //-111.936779022217, 33.4263644371816
					        			  //String url = String.format("http://joshpierro.cartodb.com/api/v2/sql?q=SELECT cartodb_id id,name,st_asgeojson(the_geom),ST_Distance(ST_Transform(ST_SetSRID(ST_Point(%s, %s),4326),3785),ST_Transform(the_geom_webmercator,3785)) Distance FROM features WHERE ST_Distance(ST_Transform(ST_SetSRID(ST_Point(%s, %s),4326),3785),ST_Transform(the_geom_webmercator,3785)) < 100",Double.toString(mLongitude),Double.toString(mLatitude),Double.toString(mLongitude),Double.toString(mLatitude));
					        			  String query = String.format("q=SELECT cartodb_id id,name,st_asgeojson(the_geom),ST_Distance(ST_Transform(ST_SetSRID(ST_Point(%s, %s),4326),3785),ST_Transform(the_geom_webmercator,3785)) Distance FROM features WHERE ST_Distance(ST_Transform(ST_SetSRID(ST_Point(%s, %s),4326),3785),ST_Transform(the_geom_webmercator,3785)) < 150",Double.toString(mLongitude),Double.toString(mLatitude),Double.toString(mLongitude),Double.toString(mLatitude));
					        			  
					        			  
					        			  URI uri;
					        			  String request;
					        			 //clean up  
										try {
											uri = new URI(
												        "http", 
												        "joshpierro.cartodb.com", 
												        "/api/v2/sql",
												        query,
												        null);
											request = uri.toASCIIString();
											
											//Log.d("xxx", request);
											StringBuilder builder = new StringBuilder();
										    HttpClient client = new DefaultHttpClient();
										    HttpGet httpGet = new HttpGet(request);
										    
										    try{
										        HttpResponse response = client.execute(httpGet);
										        StatusLine statusLine = response.getStatusLine();
										        int statusCode = statusLine.getStatusCode();
										        
										        if (statusCode == 200) {
											          HttpEntity entity = response.getEntity();
											          InputStream content = entity.getContent();
											          BufferedReader reader = new BufferedReader(new InputStreamReader(content));
											          String line;
											          while ((line = reader.readLine()) != null) {
											            builder.append(line);
											          }
											        } else {
											          Log.e(MainActivity.class.toString(), "Failed to download file");
											        }
											      }catch (ClientProtocolException e) {
										        e.printStackTrace();
										      } catch (IOException e) {
										        e.printStackTrace();
										      }
										    
										
										    try{
										    	String data  = builder.toString();
										    	// Log.d("xxx", data);
										    	 
										    	 JSONObject jObject = new JSONObject(data);
										         JSONArray jsonArray = jObject.getJSONArray("rows");
										      
										         Log.i(LocationListeningService.class.getName(),
										                "Number of entries " + jsonArray.length());
										        
										      
										         if(jsonArray.length() == 0 && mFeatureList.size() > 0){
										        	 
										        	 for(int i=0; i < mFeatureList.size();i++){
										        		 mFeatureList.remove(i);
										        	 }
										        	 mGeofenceDetails = "";
										        	 
										         }
										         
										             
										        ArrayList<Feature> featureList = new ArrayList<Feature>();
										        StringBuilder stringBuilder = new StringBuilder();
										        
										        if(jsonArray.length() > 0){
										        	 mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.buzzer);
											    	 mMediaPlayer.start();
											    	 
										         }
										           
										        for (int i = 0; i < jsonArray.length(); i++) {
										              JSONObject jsonObject = jsonArray.getJSONObject(i);

										              Feature feature = new Feature();
										              feature.Name = jsonObject.getString("name");
										              feature.Distance = Double.parseDouble(jsonObject.getString("distance"));
										              
										              stringBuilder.append(feature.Name).append(" - ").append(Double.toString(Math.round(feature.Distance))).append(" meters\n");
										              
										              JSONObject geoJsonObject = new JSONObject(jsonObject.getString("st_asgeojson"));
										              JSONArray geoJsonArray = geoJsonObject.getJSONArray("coordinates");
										             
										              for(int j=0;j<geoJsonArray.length();j++){
										            	  
										            	
										            	  JSONArray  list = (JSONArray) geoJsonArray.get(j);
										            	 
										            	 for (int k=0; k < list.length();k++){
			
										            		 JSONArray  list2 = (JSONArray) list.get(k);
										            	
										            		 PolygonOptions featurePolygon = new PolygonOptions();
										            		 featurePolygon.strokeColor(Color.RED).fillColor(Color.BLUE).strokeWidth(4);
										            		
										            		 
										            		 for(int l=0;l<list2.length();l++){
		 
										            			 JSONArray latLngs = (JSONArray)list2.get(l);
										            			 double latitude = (double) latLngs.get(1);
											            		 double longitude = (double) latLngs.get(0);
										            			 featurePolygon.add(new LatLng(latitude, longitude));
										            			 feature.PolygonOptions = featurePolygon;

										            		 }
										            	
										            	 }
										            	
										            	  
										              }
										              
										              featureList.add(feature);
										              FeatureList.getInstance().FeatureList = featureList;
										              mFeatureList = featureList;
										              mGeofenceDetails = stringBuilder.toString();
												        
										             if(mBuilder!=null){
												            	mBuilder.setContentText(mGeofenceDetails);
												            	mNotificationManager.notify(mNotificationId, mBuilder.build());
												            	
												            //	Toast.makeText(getApplicationContext(), mGeofenceDetails, 2000).show();;
												            	
												            }
										           
										            }
										        
										        
										    
										    		}catch (Exception e){
										    	Log.d("JSON","oops " + e.toString());
										    	return;
										    }
											
											 
										} catch (URISyntaxException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
					        			  


									}
									
									
									Thread.sleep(5*1000);
									
									
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
			        	}
			        	
			        	
			        }
			    }).start();
			
			 return Service.START_STICKY;
			
		}


	public ArrayList<Feature> DownLoad(){
			return mFeatureList;		
	}



	@Override
    public void onDestroy() {
		mNotificationManager.cancel(NOTIFICATION);
    	mRunFlag = false;
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		 return mBinder;
	}
	
	private final IBinder mBinder = new LocalBinder();
	
	
	public void hideNotification(){
		mNotificationManager.cancel(mNotificationId);
	}
	
    private void showNotification() {


        mBuilder = new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.icon)
        .setContentTitle("Geofence Service Started")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(mGeofenceDetails))
        .setAutoCancel(true);

        mNotificationManager.notify(mNotificationId, mBuilder.build());
        
    }
	
	
}
