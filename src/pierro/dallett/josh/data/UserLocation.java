package pierro.dallett.josh.data;

public class UserLocation {
	
	public double Latitude;
	public double Longitude;
	
	private static UserLocation  instance = new UserLocation();
	public UserLocation(){}
	public static  UserLocation getInstance(){
		return instance;
	};

}
