package pierro.dallett.josh;

import java.util.ArrayList;

import pierro.dallett.josh.data.Feature;
import pierro.dallett.josh.data.UserLocation;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

public class MainActivity extends Activity implements LocationListener,
GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener{

	 Button mStartButton; 
	 Button mStopButton;
	 boolean mServiceFlag;
	 boolean mIsBound;
	 private LocationListeningService mBoundService;
	 
	 MapFragment mMapFragment; 
	 LocationManager mLocationManager;
	 LocationRequest mLocationRequest;
	 LocationClient mLocationClient;
	 boolean mUpdatesRequested; 
	 String mProvider;
	 
	 final int GONE = 0x00000008; 
	 final int VISIBLE = 0x00000000;
	 
	 private ServiceConnection mConnection = new ServiceConnection() {
		    public void onServiceConnected(ComponentName className, IBinder service) {
		    	
		        mBoundService = ((LocationListeningService.LocalBinder)service).getService();
		        Log.d("service binder"," connected");
		    }

		    public void onServiceDisconnected(ComponentName className) {
		        mBoundService = null;
		       
		    }
		};
	 
		void doBindService() {
		    bindService(new Intent(this, 
		    		LocationListeningService.class), mConnection, Context.BIND_AUTO_CREATE);
		    mIsBound = true;
		}
	 
		void doUnbindService() {
		    if (mIsBound) {
		        unbindService(mConnection);
		        mIsBound = false;
		        Log.d("service binder"," disconnected");
		    }
		}
		
		@Override
		protected void onDestroy() {
		    super.onDestroy();
		    doUnbindService();
		}
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);	

		 mStartButton = (Button) findViewById(R.id.service_start_button);
		 mStopButton = (Button) findViewById(R.id.service_stop_button); 
		 mStopButton.setVisibility(GONE);

		mStartButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				mUpdatesRequested = true;
				
				
				mStopButton.setVisibility(VISIBLE);
				mStartButton.setVisibility(GONE);
				mServiceFlag = true; 
		        startService(new Intent(getApplicationContext(), LocationListeningService.class));
			    doBindService();
			    
	
			}
        });
        
		
		
		
		
	  mStopButton.setOnClickListener(new OnClickListener() {
		  
			@Override
			public void onClick(View v) {

				mStartButton.setVisibility(VISIBLE);
				mStopButton.setVisibility(GONE);
				mServiceFlag = false; 
				mMapFragment.getMap().clear();
		
			  	if (mIsBound) {
			  		mBoundService.hideNotification();
			  	}

				stopService(new Intent(getApplicationContext(), LocationListeningService.class));
				doUnbindService();
			}
		});
	  
	
	  mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
      Criteria criteria = new Criteria();
      mProvider = mLocationManager.getBestProvider(criteria, false);

      mLocationRequest = LocationRequest.create();
      mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
      mLocationRequest.setInterval(4000);
      mLocationRequest.setFastestInterval(1000);
      
      mLocationClient = new LocationClient(this, this, this);
      mUpdatesRequested = false;
      
      //mLocationManager.requestLocationUpdates(mProvider, 400, 1, this);
      Location location =  mLocationManager.getLastKnownLocation(mProvider);
      
      mMapFragment = (MapFragment) this.getFragmentManager().findFragmentById(R.id.mapFragment);
      mMapFragment.getMap().getUiSettings().setZoomControlsEnabled(true);  
      mMapFragment.getMap().setMyLocationEnabled(true);
      

      // Initialize location 
      if (location != null) {
        onLocationChanged(location);

      } else {
      	Toast.makeText(MainActivity.this, "Location not available", Toast.LENGTH_SHORT).show();
      }
	  
	}
	
	  @Override
	    protected void onStart() {
		   super.onStart();
	        mLocationClient.connect();
	    }
	
	@Override
    protected void onResume(){
    	 super.onResume();   	 
        // mLocationManager.requestLocationUpdates(mProvider, 400, 1, this);
         mLocationClient.connect();
 	
    }


	
	@Override
	public void onLocationChanged(Location location) {
 

		   double latitude  = location.getLatitude();
		   double longitude = location.getLongitude();
		   LatLng latLng = new LatLng(latitude, longitude); 
		   
	        mMapFragment.getMap().clear();       
	        mMapFragment.getMap().moveCamera(CameraUpdateFactory.newLatLng(latLng));
	        mMapFragment.getMap().addCircle(new CircleOptions()
		       .center(latLng)
		       .radius(10)
		       .strokeColor(Color.BLUE)
		       .strokeWidth(1)
		       .fillColor(0x7a00ff00));
	        
	        mMapFragment.getMap().animateCamera(CameraUpdateFactory.zoomTo(16));
	        mMapFragment.getMap().addCircle(new CircleOptions()
		       .center(latLng)
		       .radius(150)
		       .strokeColor(Color.RED)
		       .strokeWidth(4)
		       .fillColor(0x7aff0000));
	     
	       
	       UserLocation.getInstance().Latitude = latitude;
	       UserLocation.getInstance().Longitude = longitude;
	       


	  	if (mIsBound && mBoundService.DownLoad()!=null) {
	  		 
	  		
	  		try {

	  			  ArrayList<Feature> featureList = mBoundService.DownLoad();    
			      for (int i =0;i<featureList.size();i++){
			    	
			    	  Log.i("features",featureList.get(i).Name);
			    	  mMapFragment.getMap().addPolygon(featureList.get(i).PolygonOptions); 
			    	  
			    	  
			      }
			      
			      
			      
			} catch (Exception e) {
				e.printStackTrace();
			}
	  	
		}
	      	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		 if (mUpdatesRequested) {
	            mLocationClient.requestLocationUpdates(mLocationRequest, this);
	        }
		
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}

}
